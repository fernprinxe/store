# store recovery

[![store](https://img.shields.io/badge/license-BSD3--clause%20license-brightgreen)](https://en.wikipedia.org/wiki/BSD_licenses#3-clause_license_(%22BSD_License_2.0%22,_%22Revised_BSD_License%22,_%22New_BSD_License%22,_or_%22Modified_BSD_License%22))

## for

windows store recovery package for version ltsc 2019.  

## packages

- Microsoft.NET*  
- Microsoft.VCLibs*  
- Microsoft.Advertising*  
- Microsoft.WindowsStore*_x64_*  
- Microsoft.WindowsStore*  
- Microsoft.StorePurchaseApp*_x64_*  
- Microsoft.StorePurchaseApp*  
- Microsoft.XboxIdentityProvider*_x64_*  
- Microsoft.XboxIdentityProvider*  

## from

moeclub: https://moeclub.org/2018/11/26/697  